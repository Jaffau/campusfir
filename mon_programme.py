def pattern(line,N):
    symboles="*"*(2*line+1)
    x=(3*N-len(symboles))//2
    print(" "*x+"  "+symboles+"  "+" "*x)
def pattern2(line,N):
    symboles="*"*(2*line+1)
    x=(3*N-len(symboles))//2
    print(" "*x+" 0"+symboles+"0 "+" "*x)
def pattern3(line,N):
    symboles="*"*(2*line+1)
    x=(3*N-len(symboles))//2
    print(" "*x+"0*"+symboles+"*0"+" "*x)
def pattern4(line,N):
    symboles="*"
    x=((3*N-len(symboles))//2)
    print(" "*x+"**"+symboles+"**"+" "*x)
def mat(N):
    for line in range(N//2):
        pattern(line,N)
    for line in range(N//2):
        pattern2(line,N)
    for line in range(N//2):
        pattern3(line,N)
    for line in range(N//2):
        pattern4(line,N)


N=int(input("Size?"))
print(f"Door mat : {N}")
mat(N)